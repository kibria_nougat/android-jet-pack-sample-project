package bd.com.mcc.databindingexample;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

import bd.com.mcc.databindingexample.adapter.MyMoviesAdapter;
import bd.com.mcc.databindingexample.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();
    private MyMoviesAdapter adapter;
    private MovieListViewModel viewModel;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(MovieListViewModel.class);

        adapter = new MyMoviesAdapter(this);
        binding.setMyAdapter(adapter);
        binding.setLifecycleOwner(this);
        binding.setViewModel(viewModel);
        binding.setShow(true);
        getData();
    }

    public void getData() {
        viewModel.getMovieListObservable().observe(this, new Observer<ArrayList<Movie>>() {
            @Override
            public void onChanged(@Nullable ArrayList<Movie> movies) {
                Log.d(TAG, "MainActivity: " + movies.size());
                adapter.setData(movies);
                binding.setShow(false);
            }
        });
    }


}
