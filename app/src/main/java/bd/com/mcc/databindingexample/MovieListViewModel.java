package bd.com.mcc.databindingexample;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;

import bd.com.mcc.databindingexample.networking.InvokeApi;

public class MovieListViewModel extends AndroidViewModel {

    private final String TAG = MovieListViewModel.class.getSimpleName();

    private LiveData<ArrayList<Movie>> movieListObservable;
    public MediatorLiveData<Boolean> visibility;

    public MovieListViewModel(@NonNull Application application) {
        super(application);
        movieListObservable = InvokeApi.getInstance().getMovieList();
    }

    public LiveData<ArrayList<Movie>> getMovieListObservable() {
        Log.d(TAG, "onResponse: " + "getMovieListObservable");
        return movieListObservable;
    }

}
