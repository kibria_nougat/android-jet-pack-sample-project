package bd.com.mcc.databindingexample.networking;

import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import java.util.ArrayList;

import bd.com.mcc.databindingexample.Movie;
import bd.com.mcc.databindingexample.MovieResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvokeApi {

    private final String TAG = InvokeApi.class.getSimpleName();
    private static InvokeApi invokeApi;
    private static final String API_KEY = "9a4176b10acbfa14bf4740762a7a4ecb";

    private InvokeApi() {
    }

    public static InvokeApi getInstance() {
        if (invokeApi == null) {
            invokeApi = new InvokeApi();
        }

        return invokeApi;
    }


    public MutableLiveData<ArrayList<Movie>> getMovieList() {
        final MutableLiveData<ArrayList<Movie>> data = new MutableLiveData<>();
        ApiClient.getRetrofit().create(ApiInterface.class).getMovies(API_KEY).enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                Log.d(TAG, "onResponse: " + response.body().getResults().size());
                data.setValue(response.body().getResults());
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.toString());
            }
        });
        return data;
    }
}
