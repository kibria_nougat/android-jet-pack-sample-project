package bd.com.mcc.databindingexample.networking;


import bd.com.mcc.databindingexample.MovieResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET("discover/movie")
    Call<MovieResponse> getMovies(@Query("api_key") String api_key);
}
