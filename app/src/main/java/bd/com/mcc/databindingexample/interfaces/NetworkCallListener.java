package bd.com.mcc.databindingexample.interfaces;

import bd.com.mcc.databindingexample.MovieResponse;

public interface NetworkCallListener {
    void onComplete(MovieResponse data);
    void onFailed(String data);
}
