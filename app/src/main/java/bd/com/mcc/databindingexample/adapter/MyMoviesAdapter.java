package bd.com.mcc.databindingexample.adapter;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import bd.com.mcc.databindingexample.Movie;
import bd.com.mcc.databindingexample.R;
import bd.com.mcc.databindingexample.databinding.MovieRowLayoutBinding;

public class MyMoviesAdapter extends RecyclerView.Adapter<MyMoviesAdapter.ViewHolder> {

    private final String TAG = MyMoviesAdapter.class.getSimpleName();

    private ArrayList<Movie> movieArrayList = new ArrayList<>();
    private Context mContext;

    public MyMoviesAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public void setData(ArrayList<Movie> movieArrayList) {
        if (this.movieArrayList.size() > 0) {
            this.movieArrayList.clear();
        }
        this.movieArrayList.addAll(movieArrayList);
        Log.d(TAG, "setData: " + this.movieArrayList.size());
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        MovieRowLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()), R.layout.movie_row_layout, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Log.d(TAG, "onBindViewHolder: " + movieArrayList.get(i).getOriginalTitle());
        viewHolder.bind(movieArrayList.get(i));
    }

    @Override
    public int getItemCount() {
        return movieArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private MovieRowLayoutBinding binding;

        public ViewHolder(MovieRowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Movie item) {
            binding.setMovie(item);
            binding.executePendingBindings();
        }
    }

    @BindingAdapter("imageUrl")
    public static void loadImage(ImageView view, String url) {
        Glide.with(view.getContext())
                .load("https://image.tmdb.org/t/p/w500/" + url)
                .into(view);

    }

}
